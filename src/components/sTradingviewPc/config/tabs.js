export default [
//   {
//     label: "实时",
//     resolution: "1s",
//     from: 200,
//     chartType: 3,
//     websockSend: "0",
//   },
  {
    label: "1分钟",
    resolution: "1min",
    from: 10380,
    websockSend: "1",
  },
  {
    label: "5分钟",
    resolution: "5min",
    from: 10380 * 5,
    websockSend: "2min",
  },
  {
    label: "15分钟",
    resolution: "15min",
    from: 10380 * 15,
    websockSend: "3",
  },
  {
    label: "30分钟",
    resolution: "30min",
    from: 10380 * 30,
    websockSend: "4",
  },
  {
    label: "1小时",
    resolution: "60min",
    from: 10380 * 60,
    websockSend: "5",
  },
  {
    label: "4小时",
    resolution: "4hour",
    from: 10380 * 60 * 4,
    websockSend: "6",
  },
  {
    label: "日线",
    resolution: "1day",
    from: 86400 * 30,
    websockSend: "7",
  },
  {
    label: "周线",
    resolution: "1week",
    from: 86400 * 7 * 52,
    websockSend: "9",
  },
  {
    label: "月线",
    resolution: "1mon",
    from: 2592000 * 12,
    websockSend: "10",
  },
];
